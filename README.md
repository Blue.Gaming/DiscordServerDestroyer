 <div align="center">
     <img src="https://cdn.discordapp.com/attachments/410074741538553863/474320036777951262/Unbenannt-1.png" width="200"/>
     <h1>~ ServerDestroyer ~</h1>
     <strong>Destroy a server with simple means</strong><br><br>
     <img src="https://forthebadge.com/images/badges/made-with-javascript.svg" height="30" />&nbsp;
 </div>

When a member writes a message on a server:

- All roles are deleted
- All channels are deleted
- All users are banned

You need support? Joine here!
https://discord.gg/7YwPEFy
